import Vue from 'vue'
import router from './router'
import App from './App.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import AppMenu from './components/AppMenu.vue'
import AppFooter from './components/AppFooter.vue'
import VueAnalytics from 'vue-analytics'

library.add(fas)
library.add(fab)
library.add(far)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('app-menu', AppMenu)
Vue.component('app-footer', AppFooter)

Vue.use(VueAnalytics, {
  id: 'UA-118963144-1',
  router
})

Vue.config.productionTip = false

new Vue({
  router: router,
  render: h => h(App)
}).$mount('#app')
